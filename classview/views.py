from django.shortcuts import render
from django.views.generic import View
from django.http.response import HttpResponse
from django.utils.decorators import method_decorator


# Create your views here.
# GET /template_demo/
def template_demo(request):
    """演示模板加载"""
    context = {
        'name': 'cg',
        'alist': [2, 1, 5],
        'adict': {'age': 25}
    }
    return render(request, 'index.html', context=context)


def my_decorator(view_func):
    """定义装饰器装饰视图"""

    def wrapper(request, *args, **kwargs):
        print('装饰器被调用了')
        print(request.method)

        # 调用被装饰的视图
        return view_func(request, *args, **kwargs)

    return wrapper

# @my_decorator
# def demo_test(request):
#     return HttpResponse('ok')


# method_decorator(要被转换装饰器, name=要装饰的方法) 如果写在类的上面时,name参数必须指定,如果写在内部方法上时,name不需要指定
# @method_decorator(my_decorator, name='get')
class DemoView(View):
    """类视图"""

    @method_decorator(my_decorator)
    def get(self, request):
        """get请求业务逻辑"""
        return HttpResponse('get请求业务逻辑')

    def post(self, request):
        """post请求业务逻辑"""
        return HttpResponse('post请求业务逻辑')

