"""demo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.documentation import include_docs_urls

from users import views

urlpatterns = [
    # 自动生成接口文档
    url(r'^docs/', include_docs_urls(title='传智图书API')),


    url(r'^admin/', admin.site.urls),

    # 把users应用中的所有路由信息包含到总路由中
    # url(r'^users/', include('users.urls'))

    # 只在总路由中去定义路由
    # url(r'^users/index/$', views.index),

    # 只在总路由中包含子应用的路由
    url(r'^', include('users.urls', namespace='users')),

    # 包含子应用
    url(r'^', include('request_response.urls', namespace='request_response')),

    # classview
    url(r'^', include('classview.urls')),
    # 图书
    url(r'^', include('booktest.urls')),

]
