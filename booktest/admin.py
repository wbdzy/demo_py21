from django.contrib import admin

from .models import BookInfo, HeroInfo


# Register your models here.
class HeroInfoStackInline(admin.TabularInline):
    """关联展示信息"""
    model = HeroInfo  # 要关联展示的模型
    extra = 1  # 额外的编辑框 数量


class BookInfoAdmin(admin.ModelAdmin):
    """BookInfo模型管理类: 管理它在admin站点上的显示信息"""
    list_per_page = 2  # 每页显示多少条数据
    actions_on_bottom = True  # 添加底部动作选项
    actions_on_top = False  # 头部功能选项

    # 控制列表页面展示的那些列
    list_display = ['id', 'btitle', 'pub_date_format', 'bread', 'bcomment', 'is_delete']
    ordering = ['btitle']

    # fields = ['btitle', 'bpub_date', 'is_delete']  # 控制列表界面所展示的字段,默认全部展示
    fieldsets = [
        ['基本', {'fields': ['btitle', 'bpub_date', 'is_delete', 'image']}],
        ['高级', {'fields': ['bread', 'bcomment'], 'classes': ['collapse']}]
    ]

    inlines = [HeroInfoStackInline]  # 在图书的编辑界面到当前这本书关联的英雄都展示出来


@admin.register(HeroInfo)
class HeroInfoAdmin(admin.ModelAdmin):
    """控制HeroInfo"""
    # list_per_page = 10
    list_display = ['id', 'hname', 'hcomment', 'hbook', 'is_delete', 'book_read']

    # 过滤数据
    list_filter = ['hbook']
    # 搜索框
    search_fields = ['hname']



admin.site.register(BookInfo, BookInfoAdmin)  # 在admin站点中注册模型
# admin.site.register(HeroInfo, HeroInfoAdmin)


admin.site.site_header = '红袜子书城'
admin.site.site_title = '红袜子书城MIS'
admin.site.index_title = '欢迎使用红袜子书城MIS'
