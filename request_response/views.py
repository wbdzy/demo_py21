from django.shortcuts import render, reverse, redirect
from django.http.response import HttpResponse, JsonResponse, HttpResponseBadRequest
import json


# Create your views here.
# GET /session_demo/
def session_demo(request):
    """演示session读取"""
    # 设置session
    # 当去设置session时,内部会自动生成一个sessionid 并且把这个sessionid 通过后面的response对象响应给浏览器设置到cookie
    # request.session['name'] = 'cg'
    # session是依赖cookie

    # 读取seesion   当我们读取session是,先从request中拿cookie中的sessionid,通过id取出session这条记录,再用get(name)取出value
    print(request.session.get('name'))

    return HttpResponse('session_demo')

# GET /cookie_demo/
def cookie_demo(request):
    """演示cookie读写"""

    # 设置cookie
    response = HttpResponse('cookie_demo')  # 创建响应对象
    # 设置cookie   set_cookie(key, value, 过期时间单秒(秒))
    response.set_cookie('name', 'cg', max_age=3600)

    # 读取cookie
    print(request.COOKIES.get('name'))


    return response

# GET /redirect_demo/
def redirect_demo(request):
    """演示重定向"""
    # 路由命名空间作用:就是限定反向解析时,查找的范围,如果不指定命名空间,默认全局搜索,指定了命名空间就是局部搜索
    # print(reverse('request_response:index'))

    # 在重定向时,如果路径前面加/表示重定向到根路径,而不是从当前路径去拼接
    # return redirect('/users/index/')
    return redirect(reverse('users:index'))
    # return HttpResponse('redirect_demo')


# GET /json_response/
def json_response(request):
    """演示响应json数据"""
    # JSON字典中的引号必须要用双引号
    json_dict = {"name": "cg", "age": 20}
    list1 = [{"name": "cg", "age": 20}, {"name": "cg", "age": 20}]
    # return JsonResponse(json_dict)
    # return JsonResponse(list1, safe=False)
    # return HttpResponseBadRequest()


# GET /response_demo/
def response_demo(request):
    # HttpResponse(content=响应体, content_type=响应体数据类型, status=状态码)
    # 必须传递响应体,其它可以用默认的
    # return HttpResponse('response_demo', content_type='text/plain', status=201)
    response = HttpResponse('response_demo', content_type='text/plain', status=201)
    response['Itacts'] = 'Python'  # 自定义响应头
    return response























# GET /get_request_head/
def get_request_head(request):
    """演示获取请求头信息"""
    print(request.META['CONTENT_TYPE'])
    print(request.user)
    return HttpResponse('get_request_head')

# POST get_json/
def get_json(request):
    json_str_bytes = request.body  # 获取出来是json字符串的字节类型
    json_str = json_str_bytes.decode()  # 把字节类型转成字符串
    dict = json.loads(json_str)  # 把字符串的json字典转你json字典

    return HttpResponse('get_json')


# POST get_form/
def get_form(request):
    """演示获取表单数据"""
    like = request.POST.get('like')
    b = request.POST.get('b')
    like_list = request.POST.getlist('like')

    return HttpResponse('get_form')


# get_query_string/?a=10&b=20&a=30
def get_query_string(request):
    """演示获取查询字符串数据"""
    # 注意点:request.GET 后面的GET只是一个属性而已和请求方法无关
    a = request.GET.get('a')
    b = request.GET.get('b')
    a_list = request.GET.getlist('a')

    print(a, b, a_list)

    return HttpResponse('get_query_string')


# /weather/beijing/2018
# url(r'^weather/(?P<city>[a-z]+)/(?P<year>\d{4})/$', views.weather1),
# 如果用了正则组起别名,那么形参的名字必须要和别名一致,顺序可以随便
def weather2(request, city, year):
    """提取正则组中关键字参数"""
    print(city)
    print(year)

    return HttpResponse('ok')


# url(r'^weather/([a-z]+)/(\d{4})/$', views.weather1),
def weather1(request, city, year):
    """提取正则组中的位置参数"""
    print(city)
    print(year)

    return HttpResponse('ok')
