from django.shortcuts import render, reverse
from django.http.response import HttpResponse


# Create your views here.

# http://127.0.0.1:8000/users/sayhello
def say_hello(request):
    return HttpResponse('say hello')

# http://127.0.0.1:8000/users/say
def say(request):
    return HttpResponse('say')


# http://127.0.0.1:8000/users/index/
def index(request):
    """
    测试视图和路由匹配
    :param request: HttpRequest类型对象,表示请求报文信息
    :return: response  HttpResponse类型的对象, 表示响应报文信息
    """
    print(reverse('users:index'))
    return HttpResponse("hello world")
